# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for all the code that will be used to control the 6 wheeled ground rover to be used for the rescue robotics competition.


### Contribution guidelines ###

Everyone will have their own branch attached to the master so do not edit master. Only edit your branch and I will merge your changes to the master 
once we all agree on the code that was written.

### Who do I talk to? ###

If you have any questions, email me at rodriguezjacob245@gmail.